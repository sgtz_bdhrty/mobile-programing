import React, {Component} from 'react';
import { StyleSheet, Text, View, StatusBar } from 'react-native';

import Login from './login';
import Biodata from './Biodata';
export default class App extends Component {
  render(){
    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor='#1c313a'
          barstyle='light-content'
          />
          <Biodata />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black',
    alignItems: 'center',
    justifyContent: 'center'
  }
});
