import React, {Component} from 'react';
import { StyleSheet, Text, View, Image, Button } from 'react-native';

export default class Biodata extends Component{
    render(){
        return(
            <View style = {styles.container}>
                <View style={{marginTop:64, alignItems:'center'}}>
                    <View style = {styles.avatarcontainer}>
                        <Image style={styles.avatar} source = {require('./assets/sigitcapruk1.png')} />
                    </View>
                    <Text style={styles.name}>Sigit Budiharto</Text>
                </View>
                <View style={styles.statsContainer}>
                    <View style={styles.stat}>
                        <Text style={styles.statAmount}></Text>
                        <Text style={styles.statTitle}></Text>
                    </View>
                    <View style={styles.stat}>
                        <Text style={styles.statAmount}></Text>
                        <Text style={styles.statTitle}></Text>
                    </View>
                    <View style={styles.stat}>
                        <Text style={styles.statAmount}></Text>
                        <Text style={styles.statTitle}></Text>
                    </View>
                </View>
                <View style={styles.status}>
                    <Text style={styles.study}>Program study : </Text>
                        <Text style ={styles.state}>Teknik Informatika</Text>
                </View>
                <View style={styles.class}>
                    <Text style={styles.kelas}>kelas : </Text>
                    <Text style ={styles.state}>Pagi A</Text>
                </View>
                <View style={styles.instagram}>
                    <Text style={styles.ig}>Instagram :</Text>
                    <Text style ={styles.state}>sgtz_bdhrty</Text>
                </View>
                <View style={styles.tempattanggallahir}>
                    <Text style={styles.ttl}>Tempat Tanggal Lahir :</Text>
                    <Text style ={styles.state}>Purwakarta, 27-03-00</Text>
                </View>
            </View>
        )
    }
} 
const styles = StyleSheet.create({
    container:{
        backgroundColor: 'white',
        flex:1,
        width: 500
    },
    avatarcontainer: {
        shadowColor:'#151734',
        shadowRadius: 15,
        shadowOpacity: 0.4
    },
    avatar: {
        width: 136,
        height: 136,
        borderRadius: 68
    },
    name: {
        color: 'black',
        marginTop: 24,
        fontSize: 20,
        fontWeight: 'bold'
    },
    statsContainer: {
        flexDirection: "row",
        justifyContent: 'space-between',
        margin: 50,
        bottom: 20
    },
    stat: {
        alignItems: 'center',
        flex : 1
    },
    statAmount:{
        color:'#4F566D',
        fontSize: 18,
        fontWeight: 'bold'
    },
    statTitle: {
        fontSize: 16,
        color: 'black',
        fontWeight: '300',
        marginTop: 4
    },
    status: {
        bottom: 100,
        left: 20
    },
    study:{
        justifyContent: 'flex-end',
        color: 'black',
        fontSize: 30,
        fontWeight: 'bold',
    },
    class: {
        bottom:80,
        left: 20
    },
    kelas:{
        fontWeight: 'bold',
        fontSize : 30
    },
    instagram: {
        bottom : 60,
        left : 20
    },
    ig: {
        fontSize: 30,
        fontWeight: 'bold'
    },
    tempattanggallahir:{
        bottom: 30,
        left : 20
    },
    ttl: {
        fontWeight :'bold',
        fontSize: 30
    },
    state : {
        fontSize : 25,
        left : 10,
        bottom :-15
    }
});